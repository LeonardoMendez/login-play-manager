/* eslint-disable no-undef */
import "reflect-metadata";
import express from "express";
import routes from "./src/routes";
import { GeneralError } from "./src/utils/error";
import { PUERTO } from "./src/utils/config-env";
import cors from "cors";
import swaggerUi from "swagger-ui-express";
import { ILogObject, Logger } from "tslog";
import { appendFileSync } from "fs";

function logToTransport(logObject: ILogObject) {
  appendFileSync("logs.txt", JSON.stringify(logObject) + "\n");
}
const logger: Logger = new Logger({ name: "index" });
logger.attachTransport(
  {
    silly: logToTransport,
    debug: logToTransport,
    trace: logToTransport,
    info: logToTransport,
    warn: logToTransport,
    error: logToTransport,
    fatal: logToTransport,
  },
  "debug"
  );


const app = express();

app.use(cors());

/*const handleErrors = (err: Error, req: Request, res: Response) => {
    if (err instanceof GeneralError) {
        return res.status(err.getCode()).json({
            status: "error",
            message: err.message
        });
    }
  
    return res.status(500).json({
        status: "error",
        message: err.message
    });
};*/
app.use(
  (req, res, next) => {
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
      next();
  }
)


app.use(express.json());

// para el swagger
app.use(
    "/docs",
    swaggerUi.serve,
    swaggerUi.setup(undefined, {
      swaggerOptions: {
        url: "public/swagger.json",
      },
    })
  );

app.use(routes);
//app.use(handleErrors);

app.listen(PUERTO, () => {
    console.log("Server started", PUERTO);
    logger.debug("Server started", PUERTO)
});