/* eslint-disable no-undef */
import { Service } from "typedi";
import express from "express";
import RoleController  from "../controller/role.controller";

const roleRouter = express.Router();

@Service()
export default class RouterRole {
    public roleRouter = roleRouter;
    // eslint-disable-next-line no-unused-vars
    constructor(private readonly controller: RoleController) {
        this.initialize();
    }
    initialize(){
        this.roleRouter.get("/", (req, res) => this.controller.getAll(req, res));
        this.roleRouter.get("/:id", (req, res) => this.controller.getById(req, res));        
        this.roleRouter.post("/", (req, res) => this.controller.create(req, res));
        this.roleRouter.put("/:pId", (req, res) => this.controller.updateById(req, res));
        this.roleRouter.delete("/:pId", (req, res) => this.controller.deleteById(req, res));        
    }
}