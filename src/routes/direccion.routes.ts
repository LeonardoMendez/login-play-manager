/* eslint-disable no-undef */
import { Service } from "typedi";
import express, { Request, Response, NextFunction } from "express";
import DireccionController  from "../controller/direccion.controller";
import jwt from "jsonwebtoken";


const direccionRouter = express.Router();

@Service()
export default class RouterDireccion {
    public direccionRouter = direccionRouter;
    // eslint-disable-next-line no-unused-vars
    constructor(private readonly controller: DireccionController) {
        this.initialize();
    }

    verifyToken = (req: Request, res: Response, next: NextFunction) => {
        const token =
          req.body.token || req.query.token || req.headers["authorization"];
      
        if (!token) {
            return res.status(403).send("A token is required for authentication");
        }
        try {
            const decoded = jwt.verify(token.split(" ")[1], "SECRETA");
            req.body.user = decoded;
        } catch (err) {
            return res.status(401).send("Invalid Token");
        }
        return next();
    };


    initialize(){
        //direccionRouter.use(this.verifyToken);
        this.direccionRouter.get("/", (req, res) => this.controller.getAll(req, res));
        this.direccionRouter.get("/:id", (req, res) => this.controller.getById(req, res));        
        this.direccionRouter.post("/", (req, res) => this.controller.create(req, res));
        this.direccionRouter.put("/:pId", (req, res) => this.controller.updateById(req, res));
        this.direccionRouter.delete("/:pId", (req, res) => this.controller.deleteById(req, res));        
        //this.direccionRouter.get("/user_id/:pUser_id", (req, res) => this.controller.getByUserId(req, res));
    }
}