import express from "express";
import { Container } from "typedi";
import RouterUser from "./user.routes";
import RouterRole from "./role.routes";
import RouterDireccion from "./direccion.routes";

const router = express.Router();
const userRoutes = Container.get<RouterUser>(RouterUser);
const roleRoutes = Container.get<RouterRole>(RouterRole);
const direccionRoutes = Container.get<RouterDireccion>(RouterDireccion);

router.use("/user", userRoutes.userRouter);
router.use("/role", roleRoutes.roleRouter);
router.use("/direccion", direccionRoutes.direccionRouter);

export default router;