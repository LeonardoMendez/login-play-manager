/* eslint-disable no-undef */
import { Service } from "typedi";
import express, { Request, Response, NextFunction } from "express";
import UserController  from "../controller/user.controller";
import AuthController from "../controller/auth.controller";
import jwt from "jsonwebtoken";

const userRouter = express.Router();

@Service()
export default class RouterUser {
    public userRouter = userRouter;
    // eslint-disable-next-line no-unused-vars
    constructor(private readonly controller: UserController, private readonly authController: AuthController) {
        this.initialize();
    }

    verifyToken = (req: Request, res: Response, next: NextFunction) => {
        const token =
          req.body.token || req.query.token || req.headers["authorization"];
      
        if (!token) {
            return res.status(403).send("A token is required for authentication");
        }
        try {
            const decoded = jwt.verify(token.split(" ")[1], "SECRETA");
            req.body.user = decoded;
        } catch (err) {
            return res.status(401).send("Invalid Token");
        }
        return next();
    };

    initialize(){
        this.userRouter.post("/register", (req, res, next: NextFunction) => this.authController.register(req, res, next));
        this.userRouter.post("/login", (req, res, next: NextFunction) => this.authController.login(req, res, next));
        userRouter.use(this.verifyToken);
        //this.userRouter.get("/", (req, res) => this.controller.getAll(req, res));
        this.userRouter.get("/:id", (req, res) => this.controller.getById(req, res));        
        this.userRouter.put("/:pId", (req, res) => this.controller.updateById(req, res));
        this.userRouter.delete("/:pId", (req, res) => this.controller.deleteById(req, res));        
        this.userRouter.get("/:pUser_id/direcciones", (req, res) => this.controller.getDireccionesByUserId(req, res));
    }
}