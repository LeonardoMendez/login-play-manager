import {Service} from "typedi";
import UserModel, { UserCreate, UserView } from "../models/user.model";
import associations from "../models/associations";

@Service()
export default class UserDao {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly userModel: UserModel, private readonly associations: associations) {
        // eslint-disable-next-line no-undef
        console.log("Nueva clase de userDaoCreada");
    }

    readAll(): Promise<UserView[] | null> {
        return this.userModel.UserModelInstance.findAll({
            attributes: ["nombre"],
            include: [
                {
                    association: this.associations.roleUser, 
                    attributes: ["nombre"]
                }
            ]
        });
    }
     
    read(Id: number): Promise<UserView | null> {
        return this.userModel.UserModelInstance.findByPk(Id);
    }

    findOne(arg: any): Promise<UserView | null> {
        return this.userModel.UserModelInstance.findOne({where: arg});
    }

    create(Registro: UserCreate): Promise<UserView> {
        return this.userModel.UserModelInstance.create(Registro);
    }

    update(Id: number, Registro: UserCreate): Promise<[number, UserView[]]> {
        return this.userModel.UserModelInstance.update(Registro, {where: {id: Id}});
    }
    
    delete(Id: number): Promise<number> {
        return this.userModel.UserModelInstance.destroy({where: {id: Id}});
    }

    async getDireccionesByUserId(pUser_id: string): Promise<any> {
        let userIntance = await this.userModel.UserModelInstance.findByPk(Number(pUser_id))
        return userIntance?.getDirecciones()
    }
}