import {Service} from "typedi";
import RoleModel, { RoleCreate, RoleView } from "../models/role.model";
import associations from "../models/associations";

@Service()
export default class RoleDao {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly roleModel: RoleModel, private readonly associations: associations) {
        // eslint-disable-next-line no-undef
        console.log("Nueva clase de roleDaoCreada");
    }

    readAll(): Promise<RoleView[] | null> {
        return this.roleModel.RoleModelInstance.findAll({
            attributes: ["nombre"],
            include: [
                {
                    association: this.associations.userRole, 
                    attributes: ["nombre"]
                }
            ]
        });
    }
     
    read(Id: number): Promise<RoleView | null> {
        return this.roleModel.RoleModelInstance.findByPk(Id);
    }

    create(Registro: RoleCreate): Promise<RoleView> {
        return this.roleModel.RoleModelInstance.create(Registro);
    }

    update(Id: number, Registro: RoleCreate): Promise<[number, RoleView[]]> {
        return this.roleModel.RoleModelInstance.update(Registro, {where: {id: Id}});
    }
    
    delete(Id: number): Promise<number> {
        return this.roleModel.RoleModelInstance.destroy({where: {id: Id}});
    }
}