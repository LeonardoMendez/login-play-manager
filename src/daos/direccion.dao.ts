import {Service} from "typedi";
import DireccionModel, { DireccionCreate, DireccionView } from "../models/direccion.model";
import associations from "../models/associations";

@Service()
export default class DireccionDao {
   
    // eslint-disable-next-line no-unused-vars
    constructor(private readonly direccionModel: DireccionModel, private readonly associations: associations) {
        // eslint-disable-next-line no-undef
        console.log("Nueva clase de direccionDaoCreada");
    }

    readAll(): Promise<DireccionView[] | null> {
        return this.direccionModel.DireccionModelInstance.findAll({
            attributes: ["calle"],
            include: [
                {
                    association: this.associations.userDireccion, 
                    attributes: ["nombre"]
                }
            ]
        });
    }
     
    read(Id: number): Promise<DireccionView | null> {
        return this.direccionModel.DireccionModelInstance.findByPk(Id);
    }

    create(Registro: DireccionCreate): Promise<DireccionView> {
        return this.direccionModel.DireccionModelInstance.create(Registro);
    }

    update(Id: number, Registro: DireccionCreate): Promise<[number, DireccionView[]]> {
        return this.direccionModel.DireccionModelInstance.update(Registro, {where: {id: Id}});
    }
    
    delete(Id: number): Promise<number> {
        return this.direccionModel.DireccionModelInstance.destroy({where: {id: Id}});
    }

    
}