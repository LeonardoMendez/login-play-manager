export default class UserDTO {
    readonly id!: number;
    readonly nombre!: string;
    readonly email!: string;
    readonly password!: string;
    readonly role_id!: number
}