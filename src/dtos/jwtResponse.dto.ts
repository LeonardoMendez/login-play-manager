export default interface JwtResponseDTO {
    token: string
}
