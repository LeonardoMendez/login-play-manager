export default interface ResponseDTO {
    status: number;
    message: string;
}