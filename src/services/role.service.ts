import { Service } from "typedi";
import { RoleCreate, RoleView } from "../models/role.model";
import RoleDao from "../daos/role.dao";

@Service()
export default class RoleService {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly roleDao: RoleDao){}

    create = (Registro: RoleCreate): Promise<RoleView> => {
        return this.roleDao.create(Registro);
    };

    getById = (Id: number): Promise<RoleView | null> => {
        return this.roleDao.read(Id);
    };

    deleteById = ( Id: number ): Promise<number> => {
        return this.roleDao.delete(Id);
    };

    updateById = ( Id: number, Registro: RoleCreate ): Promise<[number, RoleView[]]> => {
        return this.roleDao.update(Id, Registro);
    }; 
    
    getAll = (): Promise<RoleView[] | null> => {
        return this.roleDao.readAll();
    };
}