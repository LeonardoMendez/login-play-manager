import { Service } from "typedi";
import { DireccionCreate, DireccionView } from "../models/direccion.model";
import DireccionDao from "../daos/direccion.dao";

@Service()
export default class RoleService {
    // eslint-disable-next-line no-unused-vars
    constructor(private readonly direccionDao: DireccionDao){}

    create = (Registro: DireccionCreate): Promise<DireccionView> => {
        return this.direccionDao.create(Registro);
    };

    getById = (Id: number): Promise<DireccionView | null> => {
        return this.direccionDao.read(Id);
    };

    // getByUserId(pUser_id: string): any {
    //     return this.direccionDao.getByUserId(pUser_id)
    // }

    deleteById = ( Id: number ): Promise<number> => {
        return this.direccionDao.delete(Id);
    };

    updateById = ( Id: number, Registro: DireccionCreate ): Promise<[number, DireccionView[]]> => {
        return this.direccionDao.update(Id, Registro);
    }; 
    
    getAll = (): Promise<DireccionView[] | null> => {
        return this.direccionDao.readAll();
    };
}