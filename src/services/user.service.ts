import { Service } from "typedi";
import { UserCreate, UserView } from "../models/user.model";
import UserDao from "../daos/user.dao";
import { ILogObject, Logger } from "tslog";
import { appendFileSync } from "fs";

const log: Logger = new Logger({ name: "USER-SERVICE" });
function logToTransport(logObject: ILogObject) {
    appendFileSync("logs.txt", JSON.stringify(logObject) + "\n");
  }
  
  const logger: Logger = new Logger();
  logger.attachTransport(
    {
      silly: logToTransport,
      debug: logToTransport,
      trace: logToTransport,
      info: logToTransport,
      warn: logToTransport,
      error: logToTransport,
      fatal: logToTransport,
    },
    "debug"
  );
@Service()
export default class UserService {

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly userDao: UserDao){}

    create = (Registro: UserCreate): Promise<UserView> => {
        try {
            return this.userDao.create(Registro);
        } catch (e: any) {
            log.warn("Fallo la creacion de Usuario", e.message + Registro);
            throw new Error("Fallo la creacion de Usuario");
        }
    };

    getById = (Id: number): Promise<UserView | null> => {
        return this.userDao.read(Id);
    };

    deleteById = ( Id: number ): Promise<number> => {
        return this.userDao.delete(Id);
    };

    updateById = ( Id: number, Registro: UserCreate ): Promise<[number, UserView[]]> => {
        return this.userDao.update(Id, Registro);
    }; 
    
    getAll = (): Promise<UserView[] | null> => {
        return this.userDao.readAll();
    };

    findOne = (arg: any): Promise<UserView | null> => {
        return this.userDao.findOne(arg);
    };

    getDireccionesByUserID = (userId: string): Promise<UserView | null> => {
        return this.userDao.getDireccionesByUserId(userId);
    };
}
