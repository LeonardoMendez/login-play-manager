import { Model, DataTypes, Optional } from "sequelize";
import SequelizeDAO from "../daos/sequelize.dao";
import { Service } from "typedi";

interface RoleAttributes {
    id: number
    nombre: string
    tipo: string
}

interface RoleInstance  extends Model<RoleAttributes>, RoleAttributes {}

export interface RoleView {
    nombre: string
    tipo: string
}

export interface RoleCreate {
    id: number
    nombre: string
    tipo: string
}

@Service()
export default class RoleModel {
    
    public RoleModelInstance;    

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly sequelize: SequelizeDAO){
        this.RoleModelInstance = this.sequelize.getSequelize().define<RoleInstance>("Roles", {
            id: {
                primaryKey: true,
                allowNull: false,
                type: DataTypes.INTEGER.UNSIGNED
            },
            nombre: {allowNull: false, type: DataTypes.STRING(40)},
            tipo: {allowNull: false, type: DataTypes.STRING(40)}
        }, {
            tableName: "Roles",
            name: {
                singular: "Roles",
                plural: "Roles"
            },
            freezeTableName: true,
            timestamps: false
        });
    }
}
