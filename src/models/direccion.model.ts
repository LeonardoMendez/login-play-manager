import { Model, DataTypes, Optional, HasManyGetAssociationsMixin } from "sequelize";
import SequelizeDAO from "../daos/sequelize.dao";
import { Service } from "typedi";

export interface DireccionAttributes {
    id: number
    calle: string
    calleNro: number
    ciudad: string
}


interface DireccionInstance  extends Model<DireccionAttributes>, DireccionAttributes {}
// export interface EquipoInstance extends Model<EquipoAttributes>, EquipoAttributes {
//     getJugadores: HasManyGetAssociationsMixin<JugadorInstance>
//     addJugadores: HasManyAddAssociationsMixin<JugadorInstance, JugadorInstance['id']>
//     setJugadores: HasManySetAssociationsMixin<JugadorInstance, JugadorInstance['id']>
//     createJugadore: HasManyCreateAssociationMixin<JugadorInstance>
//     hasJugadore: HasManyHasAssociationMixin<JugadorInstance, JugadorInstance['id']>
// }


export interface DireccionView {
    calle: string
    calleNro: number
    ciudad: string
}

export interface DireccionCreate {
    id: number
    calle: string
    calleNro: number
    ciudad: string
    user_id: number

}

@Service()
export default class DireccionModel {
    
    public DireccionModelInstance;    

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly sequelize: SequelizeDAO){
        this.DireccionModelInstance = this.sequelize.getSequelize().define<DireccionInstance>("Direcciones", {
            id: {
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
                type: DataTypes.INTEGER.UNSIGNED
            },
            calle: {allowNull: false, type: DataTypes.STRING(20)},
            calleNro: {allowNull: false, type: DataTypes.INTEGER},
            ciudad: {allowNull: false, type: DataTypes.STRING(20)},

        }, {
            tableName: "Direcciones",
            name: {
                singular: "Direcciones",
                plural: "Direcciones"
            },
            freezeTableName: true,
            timestamps: false
        });
    }
}