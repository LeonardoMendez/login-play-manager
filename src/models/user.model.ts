import { Model, DataTypes, Optional, HasManyGetAssociationsMixin } from "sequelize";
import SequelizeDAO from "../daos/sequelize.dao";
import { Service } from "typedi";
import { DireccionAttributes } from "./direccion.model";

interface UserAttributes {
    id: number
    nombre: string
    email: string
    password: string
}

export interface UserInstance  extends Model<UserAttributes>, UserAttributes {
    getDirecciones: HasManyGetAssociationsMixin<DireccionAttributes>
}

export interface UserView {
    id: number
    nombre: string
    email: string
    password: string
}

export interface UserCreate {
    id: number
    nombre: string
    email: string
    password: string
    role_id: number
}

@Service()
export default class UserModel {
    
    public UserModelInstance;    

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly sequelize: SequelizeDAO){
        this.UserModelInstance = this.sequelize.getSequelize().define<UserInstance>("Users", {
            id: {
                primaryKey: true,
                allowNull: false,
                type: DataTypes.INTEGER.UNSIGNED
            },
            nombre: {allowNull: false, type: DataTypes.STRING(40)},
            email: {allowNull: false, type: DataTypes.STRING(60)},
            password: {allowNull: false, type: DataTypes.STRING(60)}
        }, {
            tableName: "Users",
            name: {
                singular: "Users",
                plural: "Users"
            },
            freezeTableName: true,
            timestamps: false
        });
    }
}
