import { Service } from "typedi";
import UserModel from "./user.model";
import RoleModel from "./role.model";
import DireccionModel from "./direccion.model";

@Service()
export default class associations {

    public roleUser: any;
    public userRole: any;
    public userDireccion: any;
    public direccionUser: any;
  
    constructor (private readonly userModel: UserModel, 
                 private readonly roleModel: RoleModel,
                 private readonly direccionModel: DireccionModel){
        
        this.roleUser = this.userModel.UserModelInstance.belongsTo(this.roleModel.RoleModelInstance, { foreignKey: {name: 'role_id', allowNull: false}});
        this.userRole = this.roleModel.RoleModelInstance.hasMany(this.userModel.UserModelInstance, {sourceKey:"id", foreignKey: {name:"role_id", allowNull: false}});

        this.userDireccion = this.direccionModel.DireccionModelInstance.belongsTo(this.userModel.UserModelInstance, { foreignKey: {name: 'user_id', allowNull: false}});
        this.direccionUser = this.userModel.UserModelInstance.hasMany(this.direccionModel.DireccionModelInstance, {sourceKey:"id", foreignKey: {name:"user_id", allowNull: false}})
        

    }
}
