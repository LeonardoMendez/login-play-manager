/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import { Request, Response } from "express";
import DireccionService from "../services/direccion.service";
import { Service } from "typedi";
import { ParsedQs } from "qs";

@Service()
export default class DireccionController{


    // eslint-disable-next-line no-unused-vars
    constructor(private readonly direccionServices: DireccionService){

    }

    async getAll(req: Request, res: Response) {
        res.json(await this.direccionServices.getAll());
    } 

    async getById (req: Request, res: Response): Promise<void> {
        res.json(await this.direccionServices.getById(Number(req.params.id)));        
    }     

    // async getByUserId(req: Request<{ pUser_id: string; }>, res: Response): Promise<void> {
    //     res.json(await this.direccionServices.getByUserId( req.params.pUser_id ))
    // }
    
    async create(req: Request, res: Response) {
        let registro = req.body;
        res.json(await this.direccionServices.create(registro));        
    }

    async updateById(req: Request, res: Response) {
        let registro = req.body.vRegistro;
        let Id = Number(req.params.pId);
        res.json(await this.direccionServices.updateById(Id, registro));
    }

    async deleteById(req: Request, res: Response) {
        res.json(await this.direccionServices.deleteById(Number(req.params.pId)));
    }
}