/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import { Request, Response, NextFunction } from "express";
import { Service } from "typedi";
import { UserCreate } from "../models/user.model";
import UserService from "../services/user.service";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import UserDTO from "../dtos/user.dto";
import { BadRequest } from "../utils/error";

@Service()
export default class AuthController{

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly userServices: UserService){

    }

    async register(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const vReg: UserDTO = req.body;

            if (!(vReg.email && vReg.nombre && vReg.password)) {
                res.status(400).send("All input is required");
            }

            const oldUser = await this.userServices.findOne({email: vReg.email})

            if (oldUser) {
                res.status(409).send("User already exist. Please login");
            }

            const encryptedPassword = await bcrypt.hash(vReg.password, 10);
            const userCreate: UserCreate = vReg;
            userCreate.password = encryptedPassword;

            const user = await this.userServices.create(userCreate);

            const token = jwt.sign({ 
                user_id: user.id,
                nombre: user.nombre,
                email: user.email
            },
                "SECRETA",
            {expiresIn: "2h",});

            res.status(201).header({token: token}).json(user);
        }
        catch(err){
            console.log(err);
            //next (err.message)

        }
    } 

    async login(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {

            const vReg: UserDTO = req.body;

            if (!(vReg.email && vReg.password)) {
                res.status(400).send({status: 400, message: "All input is required"});
            }

            const user = await this.userServices.findOne({email: vReg.email})

            if (user && (await bcrypt.compare(vReg.password, user.password))) {
                const token = jwt.sign({ 
                    user_id: user.id,
                    nombre: user.nombre,
                    email: user.email
                },
                    "SECRETA",
                {expiresIn: "2h",});

                let jwtResponseDTO = { 
                    token: token
                }
                res.status(200).send({status: 200, message: "success!!", jwtResponseDTO });      
            } else {
                res.status(403).send({status: 403, message: "Invalid Credentials"});
            }
        } 
        catch (err) {
            console.log(err);
            //next (err.message)
        }
    }
}