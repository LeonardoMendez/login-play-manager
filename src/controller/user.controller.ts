/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import { Request, Response } from "express";
import UserService from "../services/user.service";
import { Service } from "typedi";
import { Get, Post, Route } from "tsoa";

@Service()
@Route("UserController")
export default class UserController{

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly userServices: UserService){

    }

    async getAll(req: Request, res: Response) {
        res.json(await this.userServices.getAll());
    } 

    @Get("/")
    async getMessage(req: Request, res: Response): Promise<any> {
        res.json({
            message: "pong",
           });
    }

    async getById (req: Request, res: Response): Promise<void> {
        res.json(await this.userServices.getById(Number(req.params.id)));        
    }     

    async getDireccionesByUserId(req: Request, res: Response): Promise<void> {
        res.json(await this.userServices.getDireccionesByUserID( req.params.pUser_id ))
    }
    
    async create(req: Request, res: Response) {
        let registro = req.body.vRegistro;
        res.json(await this.userServices.create(registro));        
    }

    async updateById(req: Request, res: Response) {
        let registro = req.body.vRegistro;
        let Id = Number(req.params.pId);
        res.json(await this.userServices.updateById(Id, registro));
    }

    async deleteById(req: Request, res: Response) {
        res.json(await this.userServices.deleteById(Number(req.params.pId)));
    }
}