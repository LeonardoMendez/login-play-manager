/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import { Request, Response } from "express";
import RoleService from "../services/role.service";
import { Service } from "typedi";

@Service()
export default class RoleController{

    // eslint-disable-next-line no-unused-vars
    constructor(private readonly roleServices: RoleService){

    }

    async getAll(req: Request, res: Response) {
        res.json(await this.roleServices.getAll());
    } 

    async getById (req: Request, res: Response): Promise<void> {
        res.json(await this.roleServices.getById(Number(req.params.id)));        
    }     
    
    async create(req: Request, res: Response) {
        let registro = req.body;
        res.json(await this.roleServices.create(registro));        
    }

    async updateById(req: Request, res: Response) {
        let registro = req.body.vRegistro;
        let Id = Number(req.params.pId);
        res.json(await this.roleServices.updateById(Id, registro));
    }

    async deleteById(req: Request, res: Response) {
        res.json(await this.roleServices.deleteById(Number(req.params.pId)));
    }
}